FROM golang:1.14.4 AS builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o goapp .

FROM scratch
COPY --from=builder /app/goapp .
COPY config.yml .

EXPOSE 5000
ENTRYPOINT ["./goapp"]