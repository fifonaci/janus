BINARY=janus
GOARCH=amd64
VERSION=1.0.0
BUILD_DATE=`date +%F-%T`
DEPLOY_HOST=cloudsql
TAG = $$(git rev-parse --short HEAD)
GO_LDFLAGS=-ldflags "-w -s -X main.Version=${VERSION}  -X main.BuildDate=${BUILD_DATE} -X main.GitHash=$(TAG)"

# Detect the os so that we can build proper statically linked binary
OS := $(shell uname -s | awk '{print tolower($$0)}')

# Build the project
all: clean test linux darwin run

linux:
	GOOS=linux GOARCH=${GOARCH} go build ${GO_LDFLAGS} -o bin/${BINARY}-linux-${GOARCH} .

darwin:
	GOOS=darwin GOARCH=${GOARCH} go build ${GO_LDFLAGS} -o bin/${BINARY}-darwin-${GOARCH} .

test:
	go test -v -cover ./...

# TODO: add support for linux run
run:
	./bin/${BINARY}-$(OS)-${GOARCH} -mock true
	
docker:
	docker build -t janus/${BINARY}:${GOARCH}-$(TAG) .

deploy:
	scp bin/${BINARY}-linux-${GOARCH}  ${DEPLOY_HOST}:janus/

clean:
	rm -rf bin


.PHONY: linux darwin test clean
