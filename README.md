# Janus #

[Janus](https://en.wikipedia.org/wiki/Janus) is god of transitions and gates. We have configured our continual builds with  [pyouroboros/ouroboros](https://github.com/pyouroboros/ouroboros) that tracks new Docker images in repo and upgrade to latest version. That is great but we are missing the crutial piece that is running
our integration tests for new builds.

![components](img/build-components.png)

Janus exposes [caronc/apprise](https://github.com/caronc/apprise/wiki/Notify_Custom_JSON) API that is used by pyouroboros/ouroboros for notifications, e.g. upgrade container

```
{
    "version": "1.0",
    "title": "Ouroboros has updated containers!",
    "message": "Host/Socket: cp / var/run/docker.sock\r\nContainers Monitored: 3\r\nTotal Containers Updated: 18\r\nContainers updated this pass: 1\r\nFoo updated from 00a470f8e6 to 7a31ce12ac",
    "type": "info"
}
```

![steps](img/steps.png)

## Config example
```
# Logging
logging:
    profile: prod #supported dev or prod
    # dev is configured with Debug level and console formatter
    # prod has Info level and json structured formatter

# Tests execution config
tests:
    exec_path: "./test_exec.sh" 
    # Absolute path to tests executor 
    # that returns RC 0 (pass) or RC > 0 (failure)

# Slack notification    
slack_notification:
    enabled: true
    webhook_url: Bar
    channel: Foo
```

## Prometheus Metrics

* janus_fails_total The total number of failed tests execution
* janus_request_total The total number of processed requests
* janus_running Running tests
* janus_suc_total The total number of suc. tests executions
* janus_test_duration_seconds Tests execution histogram in seconds

![Grafana dashboard](img/dashboard.png)

### Build
```
make
```

### Helm
```
helm package ./janus_helm
```
