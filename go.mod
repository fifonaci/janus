module bitbucket.org/fifonaci/janus

go 1.14

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/avast/retry-go v2.6.0+incompatible
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/prometheus/client_golang v1.6.0
	github.com/stretchr/testify v1.6.1
	go.uber.org/zap v1.15.0
	gopkg.in/yaml.v2 v2.3.0
)
