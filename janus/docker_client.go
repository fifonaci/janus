// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"context"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

const (
	// OurobosImageName is name of ourobos image
	OurobosImageName = "pyouroboros/ouroboros"
)

// Cont is simplified Container model
type Cont struct {
	// ID is container id
	ID string
	// Name is name of container
	Name string
	// State e.g. running, paused, ...
	State string
}

// DockerClientI is interface for docker management,
// e.g. container suspend/resume, ..
type DockerClientI interface {
	Suspend(containerID string) error
	Resume(containerID string) error
	ContainerInfo(containerImageName string) (Cont, error)
}

// DockerClient is client struct
type DockerClient struct {
	client client.Client
}

// NewDockerClient returns `DockerClient`
func NewDockerClient(host string) (*DockerClient, error) {
	if len(host) == 0 {
		LMissingArgument("host", host)
	}
	LDebug("Docker client init", "host", host)
	cli, err := client.NewClient(host, client.DefaultVersion, nil,
		map[string]string{"User-Agent": "janus"})
	if err != nil {
		LError("Docker client init error", "err", err)
		return &DockerClient{}, err
	}
	_, err = cli.Ping(context.Background())
	if err != nil {
		LError("Ping Failed", "err", err)
		return &DockerClient{}, err
	}
	return &DockerClient{client: *cli}, nil
}

// ContainerInfo returns running contained details
func (c *DockerClient) ContainerInfo(containerImageName string) (Cont, error) {
	filters := filters.NewArgs()
	filters.Add("ancestor", containerImageName) // Get containers started with ouroboro image
	options := types.ContainerListOptions{
		All:     true, // need to get all containers, even paused
		Filters: filters}
	containers, err := c.client.ContainerList(context.Background(), options)
	if err != nil {
		LError("List containers failed", "err", err)
		return Cont{}, err
	}
	var cont = Cont{}
	for _, c := range containers {
		LInfo("Found Container", "id", c)
		cont.ID = c.ID
		cont.Name = c.Names[0]
		cont.State = c.State
	}
	if cont.ID == "" {
		LError("Container not found!", "image", containerImageName)
	}
	return cont, nil
}

// Suspend pauses container
func (c *DockerClient) Suspend(containerID string) error {
	LInfo("Suspending container", "id", containerID)
	return c.client.ContainerPause(context.Background(), containerID)
}

// Resume unpauses container
func (c *DockerClient) Resume(containerID string) error {
	LInfo("Resuming container", "id", containerID)
	return c.client.ContainerUnpause(context.Background(), containerID)
}
