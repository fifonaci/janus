// +build integration

// Start ouroboros container as `./tests_e2e/start_outoboros.sh` !!!
// Run tests as
// `go_test -v -tags=integration -run=TestIntegDocker`
//

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package janus

import (
	"flag"
	"testing"

	"github.com/stretchr/testify/assert"
)

var host string

func init() {
	flag.StringVar(&host, "host", "unix:///var/run/docker.sock", "Docker host or unix socket")
}
func TestIntegDockerState(t *testing.T) {
	c, err := NewDockerClient(host)
	if err != nil {
		t.Fatal(err)
	}
	cont, err := c.ContainerInfo(OurobosImageName)
	if err != nil {
		t.Fatal(err)
	}
	assert.NotNil(t, cont, "Ouroboro is running")
	assert.Equal(t, "running", cont.State, "Ouroboro is running")
}

func TestIntegDockerSuspend(t *testing.T) {
	c, err := NewDockerClient(host)
	if err != nil {
		t.Fatal(err)
	}
	cont, err := c.ContainerInfo(OurobosImageName)
	if err != nil {
		t.Fatal(err)
	}
	contID := cont.ID
	err = c.Suspend(contID)
	if err != nil {
		t.Fatal(err)
	}
	// check status
	cont, err = c.ContainerInfo(OurobosImageName)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, "paused", cont.State, "Contained paused")
	err = c.Resume(contID)
	if err != nil {
		t.Fatal(err)
	}
}
