// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"bytes"
	"context"
	"os/exec"
	"syscall"
	"time"
)

const (
	// ExecTimeout is execution timeout
	ExecTimeout = 3600
)

// Executor interface for running code
// Implementations bash, mockup, ...
type Executor interface {
	Run(cmd string) (int, string)
}

// Exec ..
type Exec struct{}

// NewExecutor returns executor client
func NewExecutor() (*Exec, error) {
	return &Exec{}, nil
}

const defaultFailedCode = 1
const timeoutRCCode = 300

// Run executes cmd command
// Cancel request is supported
func (e *Exec) Run(cmd string) (int, string) {
	ctx, cancel := context.WithTimeout(context.Background(), ExecTimeout*time.Second)
	defer cancel()

	LInfo("Start command", "cmd", cmd)
	var outbuf, errbuf bytes.Buffer
	c := exec.CommandContext(ctx, cmd)
	c.Stdout = &outbuf
	c.Stderr = &errbuf

	err := c.Run()
	stdout := outbuf.String()
	stderr := errbuf.String()

	if ctx.Err() == context.DeadlineExceeded {
		LError("Tests timeouted", "Timeout", ExecTimeout)
		return timeoutRCCode, ""
	}

	var exitCode int
	if err != nil {
		// try to get the exit code
		if exitError, ok := err.(*exec.ExitError); ok {
			ws := exitError.Sys().(syscall.WaitStatus)
			exitCode = ws.ExitStatus()
		} else {
			LError("Could not get exit code for", "prg", cmd)
			exitCode = defaultFailedCode
			if stderr == "" {
				stderr = err.Error()
			}
		}
	} else {
		// success, exitCode should be 0 if go is ok
		ws := c.ProcessState.Sys().(syscall.WaitStatus)
		exitCode = ws.ExitStatus()
	}
	LInfo("Command Result", "stdout", stdout, "stderr", stderr, "exit", exitCode)
	return exitCode, stderr
}
