// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// ApprisePayload is payload defined in
// https://github.com/caronc/apprise/wiki/Notify_Custom_JSON
type ApprisePayload struct {
	Version string `json:"version"`
	Title   string `json:"title"`
	Message string `json:"message"`
	Type    string `json:"type"`
}

// The key type is unexported to prevent collisions with context keys defined in
// other packages.
type contextKey string

const (
	// APIPath is new api path
	APIPath = "/api/v1/event"
	// PingPath is ping-pong path
	PingPath = "/api/ping"
	// Timeout is Read/Write Timeouts
	Timeout = 10
	// ContextRequestID is name of header
	ContextRequestID contextKey = "requestId"
	// RequestIDHTTPHeader is HTTP header that client can pass
	RequestIDHTTPHeader = "X-Request-ID"
)

const (
	// MaxHTTPPayload is max body size in B
	MaxHTTPPayload       = 2000000
	supportedContentType = "application/json"
)

var (
	httpRequests = promauto.NewCounter(prometheus.CounterOpts{
		Name: "janus_request_total",
		Help: "The total number of processed requests",
	})
	totalFails = promauto.NewCounter(prometheus.CounterOpts{
		Name: "janus_fails_total",
		Help: "The total number of failed tests execution",
	})
	totalSuc = promauto.NewCounter(prometheus.CounterOpts{
		Name: "janus_suc_total",
		Help: "The total number of suc. tests executions",
	})
	runningTests = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "janus_running",
		Help: "Running tests",
	})
	testsDurationHistogram = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "janus_test_duration_seconds",
		Help:    "Tests execution in seconds",
		Buckets: []float64{300, 600, 900, 1200, 2400, 3600},
	})
)

type malformedRequest struct {
	status int
	msg    string
}

func (mr *malformedRequest) Error() string {
	return mr.msg
}

// PingHandler is a simple service health check
func PingHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Pong")
}

// EventCreateHandler is responsible to start tests
func (env *Env) EventCreateHandler(w http.ResponseWriter, r *http.Request) {
	var e ApprisePayload
	var requestID string
	if r.Context().Value(ContextRequestID) == nil {
		requestID = ""
	} else {
		requestID = r.Context().Value(ContextRequestID).(string)
	}
	httpRequests.Inc()
	LDebug("Event Handler",
		string(ContextRequestID), requestID)
	err := decodeRequest(w, r, &e)
	if err != nil {
		var mr *malformedRequest
		if errors.As(err, &mr) {
			http.Error(w, mr.msg, mr.status)
		} else {
			LError("Internal error",
				"err", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
		return
	}
	// Start Tests
	LRequest(
		string(ContextRequestID), requestID,
		"payload", e)

	// Parse Updated Containers
	containers, err := ParseContainers(e.Message)
	if err != nil {
		LError("Parsing error", "err", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}

	// Fire tests execution
	if e.Title == UpdateContainersTitle {
		c := make(chan UpdatedContainers)
		defer close(c)
		go env.exec(r.Context(), c)
		c <- containers
	} else {
		LInfo("Not starting tests", "title", e.Title)
	}

	w.Header().Add(RequestIDHTTPHeader, requestID)
}

func (env *Env) exec(ctx context.Context, c chan UpdatedContainers) {
	d := <-c
	env.runTests(ctx, d)
}

func (env *Env) runTests(ctx context.Context, d UpdatedContainers) {
	LInfo("Starting tests execution", string(ContextRequestID), ctx.Value(ContextRequestID))
	runningTests.Inc()
	var c Cont
	var err error
	if env.Cfg.Tests.StopUpgrades {
		c, err = env.D.ContainerInfo(OurobosImageName)
		if err != nil {
			LError("Can't get container id")
		}
		err = env.D.Suspend(c.ID)
		if err != nil {
			LError("Can't suspend ouroboros container")
		}
	}
	if env.Cfg.SlackNotification.Enabled {
		var listContainers string = "\n"
		for _, c := range d.Containers {
			listContainers = listContainers + fmt.Sprintf("- %s %s -> %s\n", c.Name, c.From, c.To)
		}
		env.N.SendMessage(ctx, OK, fmt.Sprintf("Starting tests for %s Cross fingers", listContainers))
	}
	start := time.Now()
	exitC, stderr := env.Ex.Run(env.Cfg.Tests.ExecPath)
	duration := time.Since(start)
	testsDurationHistogram.Observe(duration.Seconds())
	if exitC != 0 {
		totalFails.Inc()
		if env.Cfg.SlackNotification.Enabled {
			env.N.SendMessage(ctx, FAILED, fmt.Sprintf("Tests Failed with error message: ``` %s ```", stderr))
		}
	} else {
		totalSuc.Inc()
		if env.Cfg.SlackNotification.Enabled {
			env.N.SendMessage(ctx, OK, fmt.Sprintf("Good Job! Tests %s passed.", env.Cfg.Tests.ExecPath))
		}
	}
	runningTests.Dec()
	LInfo("Tests Execution completed", string(ContextRequestID), ctx.Value(ContextRequestID), "payload", d)
	if env.Cfg.Tests.StopUpgrades {
		err = env.D.Resume(c.ID)
		if err != nil {
			LError("Can't resume ouroboros container")
		}
	}
}

func decodeRequest(w http.ResponseWriter, r *http.Request, e *ApprisePayload) error {
	// check request type
	if r.Method != http.MethodPost {
		return &malformedRequest{status: http.StatusMethodNotAllowed, msg: "Incorrect HTTP method"}
	}

	// validate content type
	if r.Header.Get("Content-Type") != "application/json" {
		return &malformedRequest{status: http.StatusUnsupportedMediaType,
			msg: fmt.Sprintf("Supported %s only", supportedContentType)}
	}

	// allow max payload
	r.Body = http.MaxBytesReader(w, r.Body, MaxHTTPPayload)

	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	err := dec.Decode(&e)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		switch {
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)",
				unmarshalTypeError.Field, unmarshalTypeError.Offset)
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case strings.HasPrefix(err.Error(), "unexpected EOF"):
			msg := "Badly formed JSON"
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case errors.Is(err, io.EOF):
			msg := "Request body must not be empty"
			return &malformedRequest{status: http.StatusBadRequest, msg: msg}

		case err.Error() == "http: request body too large":
			msg := fmt.Sprintf("Request body must not be larger than B %d", MaxHTTPPayload)
			return &malformedRequest{status: http.StatusRequestEntityTooLarge, msg: msg}
		default:
			return err
		}
	}
	if dec.More() {
		msg := "Request body must only contain a single JSON object"
		return &malformedRequest{status: http.StatusBadRequest, msg: msg}
	}
	return nil
}
