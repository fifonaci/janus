package janus

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestResponeCode(t *testing.T) {

	parameters := []struct {
		input    string
		expected int
	}{
		{"Foo", http.StatusBadRequest},
		{"{Bar}", http.StatusBadRequest},
		{"{\"type\":\"Foo\",\"value\":1}", http.StatusBadRequest}, // missing attribute
		{"{\"version\":\"0.1\",\"title\":\"Bar\",\"message\":\"Bar\",\"type\":\"Bar\"}", http.StatusOK},
	}

	for _, p := range parameters {
		req, err := http.NewRequest(http.MethodPost, APIPath, strings.NewReader(p.input))
		req.Header.Add("Content-Type", "application/json")
		if err != nil {
			t.Fatal(err)
		}
		rr := httpRecorder(req)
		// Check the status code is what we expect.
		if status := rr.Code; status != p.expected {
			fmt.Printf("Response body %s", rr.Body.String())
			t.Errorf("handler returned wrong status code for : got %v expected %v, %s",
				status, p.expected, p.input)
		}
	}
}

func httpRecorder(req *http.Request) *httptest.ResponseRecorder {
	cfg := Config{}
	cfg.Tests.ExecPath = "ls"
	env := Env{N: &MockNotification{}, Ex: &Exec{}, Cfg: cfg}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(env.EventCreateHandler)
	handler.ServeHTTP(rr, req)
	return rr
}

func TestInvalidVerb(t *testing.T) {
	var expectedBody string = "Incorrect HTTP method"
	req, err := http.NewRequest(http.MethodGet, APIPath, nil)
	req.Header.Add("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}
	rr := httpRecorder(req)
	if status := rr.Code; status != http.StatusMethodNotAllowed {
		t.Errorf("Incorrect return code %v", status)
	}
	if body := rr.Body.String(); strings.Compare(strings.Trim(body, "\n"), expectedBody) != 0 {
		t.Errorf("Invalid body message: '%s' x '%s'", body, expectedBody)
	}
}

func TestMissingContent(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, APIPath, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httpRecorder(req)
	if status := rr.Code; status != http.StatusUnsupportedMediaType {
		t.Errorf("Incorrect return code %v", status)
	}
}
