// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParsing(t *testing.T) {

	parameters := []struct {
		input    string
		expected UpdatedContainers
		err      error
	}{
		{"Host/Socket: cp / var/run/docker.sock\r\nContainers Monitored: 3\r\nTotal Containers Updated: 18\r\nContainers updated this pass: 1\r\nresource-manager updated from 00a470f8e6 to 7a31ce12ac",
			UpdatedContainers{Socket: "cp / var/run/docker.sock", UpdatedContainersCount: 1,
				Containers: []Container{
					{Name: "resource-manager",
						From: "00a470f8e6",
						To:   "7a31ce12ac"},
				}}, nil},
		{"Host/Socket: cp / var/run/docker.sock\r\nContainers Monitored: 3\r\nTotal Containers Updated: 7\r\nContainers updated this pass: 3\r\nresource-manager updated from e7ed820a1a to 6ef13b55b6\r\nmetadata-service updated from 2003944f2a to 10a2f63d2c\r\nresource-provider updated from 8c349070fd to 2add0bcf09",
			UpdatedContainers{Socket: "cp / var/run/docker.sock", UpdatedContainersCount: 3,
				Containers: []Container{
					{Name: "resource-provider", From: "8c349070fd", To: "2add0bcf09"},
					{Name: "metadata-service", From: "2003944f2a", To: "10a2f63d2c"},
					{Name: "resource-manager", From: "e7ed820a1a", To: "6ef13b55b6"},
				}}, nil},
	}

	for _, p := range parameters {
		r, err := ParseContainers(p.input)

		if err != p.err || !assert.EqualValues(t, p.expected, r, "Incorrect Struct") {
			t.Errorf("Got: error: %v, %v expected:  %v, %v for %s",
				err, r, p.expected, p.err, p.input)
		}
	}
}

func TestSplit(t *testing.T) {
	s := "A\r\nB\r\nC\r\nD\r\nE"
	lines := SplitLines(s)
	assert.EqualValues(t, lines, []string{"A", "B", "C", "D", "E"}, "They should be equal")
	assert.Empty(t, SplitLines(""), "Return nil for empty input")
}
