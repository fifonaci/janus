// +build integration

// Run tests as
// `go_test -v -tags=integration -run=TestIntegration`
//
// Run tests
// export https_proxy=www-proxy.uk.oracle.com:80; \
// go_test -v -tags=integration -run=TestIntegration \
// -webhook="https://hooks.slack.com/services/T026HB6F7/Foo" -channel="oci-cloudsql-prague"

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package janus

import (
	"context"
	"flag"
	"testing"
)

var webhook string
var channel string

func init() {
	flag.StringVar(&webhook, "webhook", "http://hooks.slack", "Slack webhook")
	flag.StringVar(&channel, "channel", "Foo", "Slack channel")
}

func TestIntegrationCreateSlack(t *testing.T) {

	nClient, err := NewNotificationClient(webhook, channel)
	if err != nil {
		t.Fatal(err)
	}
	ctx := context.Background()
	nClient.SendMessage(ctx, OK, "Test")
	if err != nil {
		t.Fatal(err)
	}
}
