// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package janus

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/avast/retry-go"
)

// MessageType is type of message send to notification service
type MessageType int32

const (
	//OK means tests passed
	OK MessageType = 0
	//FAILED is error
	FAILED MessageType = 1

	// emoji see https://slackmojis.com/
	emoji     = ":factory:"
	goodColor = "good"
	badColor  = "danger"
	username  = "TeamCity Builder"
)

var maxRetry uint = 3

// Notification interface for sending messages
// Implementations for Slack, Mockup, SMTP, ...
type Notification interface {
	SendMessage(ctx context.Context, mType MessageType, message string) error
}

// NotificationClient is connection to notification service
type NotificationClient struct {
	webhookURL string
	channel    string
}

// NewNotificationClient return new Notification client
func NewNotificationClient(webhookURL string, channel string) (*NotificationClient, error) {
	return &NotificationClient{webhookURL: webhookURL, channel: channel}, nil
}

// SlackRequest is payalod for Slack notifcation
type SlackRequest struct {
	Channel     string        `json:"channel"`
	Username    string        `json:"username"`
	IconEmoji   string        `json:"icon_emoji"`
	Attachments []Attachments `json:"attachments"`
}

// Fields are additional fields
type Fields struct {
	Short bool `json:"short"`
}

// Attachments are message
type Attachments struct {
	Color  string   `json:"color"`
	Text   string   `json:"text"`
	Fields []Fields `json:"fields"`
}

// SendMessage is Slack implementation
func (client *NotificationClient) SendMessage(ctx context.Context, mType MessageType, msg string) error {
	var body []byte
	var responseStatus int

	LInfo("Sending Slack message",
		"webhookUrl", client.webhookURL,
		"channel", client.channel,
		"type", mType,
		"message", msg)

	var c = badColor
	if mType == OK {
		c = goodColor
	}

	request := SlackRequest{
		Channel:   client.channel,
		Username:  username,
		IconEmoji: emoji,
		Attachments: []Attachments{
			{Color: c},
			{Text: msg},
			{Fields: []Fields{
				{Short: true}}},
		},
	}

	b, err := json.Marshal(request)
	if err != nil {
		LError("Slack message creation failed", "err", err)
		return err
	}

	err = retry.Do(
		func() error {
			resp, err := http.Post(client.webhookURL, "application/json", bytes.NewBuffer(b))
			if err != nil {
				return err
			}
			defer resp.Body.Close()
			body, err = ioutil.ReadAll(resp.Body)
			if err != nil {
				return err
			}
			responseStatus = resp.StatusCode
			fmt.Println("response Status Code:", responseStatus)
			fmt.Println("response Headers:", resp.Header)
			return nil
		},
		retry.OnRetry(func(n uint, err error) {
			fmt.Printf("# %v retry %d/%d: %s \n", time.Now(), n, maxRetry, err)
		}),
		retry.Attempts(maxRetry),
	)

	if err != nil {
		return err
	}

	if responseStatus != http.StatusOK {
		LError("Sending Slack message failed",
			"responseCode", responseStatus,
			"responseBody", string(body))
		return fmt.Errorf("Slack failed")
	}
	return nil
}
