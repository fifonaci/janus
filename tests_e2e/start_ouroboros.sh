#!/bin/bash

docker run -d --rm --net=host --name ouroboros \
	-e LOG_LEVEL=debug \
	-e INTERVAL=300 \
        -e DATA_EXPORT=prometheus -e PROMETHEUS_ADDR="0.0.0.0" -e PROMETHEUS_PORT="3579" \
	-e NOTIFIERS="json://localhost:5000/api/v1/event" \
        -v /var/run/docker.sock:/var/run/docker.sock \
	pyouroboros/ouroboros
